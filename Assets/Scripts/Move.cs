using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    // Start is called before the first frame update

    public float forceValue;
    public float jumpValue;
    private Rigidbody rb;
    private AudioSource audiosource;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audiosource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && Mathf.Abs(rb.velocity.y) < 0.01f)
        {
            rb.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
            audiosource.Play();
        }

        if (Input.touchCount == 1)
        {
            if (Input.touches[0].phase == TouchPhase.Began && Mathf.Abs(rb.velocity.y) < 0.01f)
            {
                rb.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
                audiosource.Play();
            }
        }
    }

    private void FixedUpdate()
    {
        rb.AddForce(new Vector3(Input.GetAxis("Horizontal") * forceValue,0, 
            Input.GetAxis("Vertical") * forceValue));
        rb.AddForce(new Vector3(Input.acceleration.x * forceValue, 0,
            Input.acceleration.y * forceValue));
    }
}
